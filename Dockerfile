FROM ubuntu:12.04
MAINTAINER Boby Ertanto <cool.ertanto@gmail.com>

VOLUME ["/var/www"]

RUN apt-get update && \
    apt-get install -y \
      build-essential \
      libaio1 \
      unzip \
      apache2 \
      php5 \
      php5-cli \
      php-pear \
      php5-dev \
      libapache2-mod-php5

RUN apt-get clean


ADD instantclient-basic-linux.x64-12.2.0.1.0.zip /tmp/
ADD instantclient-sdk-linux.x64-12.2.0.1.0.zip /tmp/
ADD instantclient-sqlplus-linux.x64-12.2.0.1.0.zip /tmp/

RUN unzip /tmp/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sqlplus-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN ln -s /usr/local/instantclient_12_2 /usr/local/instantclient
RUN ln -s /usr/local/instantclient_12_2/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so
RUN ln -s /usr/local/instantclient_12_2/sqlplus /usr/bin/sqlplus

ENV ORACLE_HOME="instantclient,/usr/local/instantclient_12_2"
RUN echo 'instantclient,/usr/local/instantclient_12_2' | pecl install oci8-2.0.12
COPY 99-php-extra.ini /etc/php5/apache2/conf.d/
#ENTRYPOINT ["/usr/sbin/apache2", "-k", "start"]
COPY index.php /var/www

#ENV APACHE_RUN_USER www-data
#ENV APACHE_RUN_GROUP www-data
#ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 80
CMD apachectl -D FOREGROUND
